# co2mon

Sends data from co2meter.com CO₂ meters to graphite

## Installation

This script is written for python 3.5+

See https://github.com/vfilimonov/co2meter/blob/master/README.md for instructions on
installing the `co2meter` module

## Usage

Connect your CO₂ meter via USB and run:

    python3 co2mon.py --carbon-server example.com --carbon-port 2003

An example systemd unit file has been included if you wish to automatically start the
script on boot.
