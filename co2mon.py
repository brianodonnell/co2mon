#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse, co2meter, socket, time


def get_data(mon, offset=0):
    """
    Get the latest data from the sensor
    """
    if len(mon.data):
        (timestamp, ppm, temp_c) = mon.data[-1]
        return {
            "timestamp": int(timestamp.timestamp()),
            "ppm": ppm + offset,
            "temp_c": temp_c,
            "temp_f": temp_c * (9 / 5) + 32,
        }


def send_data(data, server="localhost", port=2003, tcp=False, prefix="co2mon"):
    """
    Send data to graphite
    """
    metric_ppm = "{}.ppm {} {}".format(prefix, data["ppm"], data["timestamp"])
    metric_temp_c = "{}.tempC {} {}".format(prefix, data["temp_c"], data["timestamp"])
    metric_temp_f = "{}.tempF {} {}".format(prefix, data["temp_f"], data["timestamp"])
    message = "{}\n{}\n{}\n".format(metric_ppm, metric_temp_c, metric_temp_f)
    if tcp:
        sock = socket.socket()
        sock.connect((server, port))
        sock.sendall(message.encode())
        sock.close()
    else:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(message.encode(), (server, port))


def truncate_data(mon):
    """
    Discard all but the last 10 data points to conserve memory
    """
    mon._data = mon.data[-10:]


def __main__():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--offset", default=0, help="Offset to apply to ppm readings", type=int
    )
    parser.add_argument(
        "--interval", default=10.0, help="Data sampling interval in seconds", type=float
    )
    parser.add_argument(
        "--carbon-server",
        default="localhost",
        help="Hostname/IP of the carbon/graphite server",
    )
    parser.add_argument(
        "--carbon-port",
        default=2003,
        help="Listening port of the carbon/graphite server",
    )
    parser.add_argument("--prefix", default="co2mon", help="Carbon metric prefix")
    parser.add_argument(
        "--tcp", action="store_true", default=False, help="Use TCP instead of UDP"
    )
    args = parser.parse_args()
    mon = co2meter.CO2monitor()
    mon_interval = int(args.interval / 2)
    # it takes 2-3 seconds to return a reading from the sensor
    if mon_interval < 3:
        mon_interval = 3
    mon.start_monitoring(interval=mon_interval)
    while True:
        data = get_data(mon, offset=args.offset)
        if data is not None:
            print(data)
            send_data(
                data,
                server=args.carbon_server,
                port=args.carbon_port,
                tcp=args.tcp,
                prefix=args.prefix,
            )
            truncate_data(mon)
        time.sleep(args.interval)
    mon.stop_monitoring()


__main__()
